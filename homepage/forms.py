from django import forms
from . import models

class Add_Schedule(forms.ModelForm):

	class Meta:
		model = models.Schedule
		fields = ['nama', 'kategori', 'tempat', 'hari',]
    