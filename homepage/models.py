from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Schedule(models.Model):
	nama = models.CharField(max_length=30)
	kategori = models.CharField(max_length=30)
	tempat = models.CharField(max_length=30)
	hari = models.CharField(max_length=9)
	tanggal = models.DateField(auto_now_add=True)
	waktu = models.TimeField(auto_now_add=True)

	def __str__(self):
		return self.nama
