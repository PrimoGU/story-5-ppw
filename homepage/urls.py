from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('interests/', views.interests, name='interests'),
    path('contact/', views.contact, name='contact'),
    path('add-schedule/', views.schedule_add, name='add'),
    path('list-schedule/', views.schedule_list, name='list'),
    path('del/<int:id>', views.schedule_delete, name='del'),
]