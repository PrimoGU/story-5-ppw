from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect

from .forms import Add_Schedule
from .models import Schedule

# Create your views here.

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def interests(request):
    return render(request, 'interests.html')

def contact(request):
    return render(request, 'contact.html')

def schedule_list(request):
	schedules = Schedule.objects.all().order_by('tanggal')
	return render(request, 'list_schedule.html', {'schedules' : schedules})

def schedule_add(request):
	if request.method == 'POST':
		form = Add_Schedule(request.POST)
		if form.is_valid():
			form.save()
			return redirect('homepage:list')
	else:
		form = Add_Schedule()
	return render(request, 'add_schedule.html', {'form' : form})

def schedule_delete(request, id):
	Schedule.objects.get(pk=id).delete()
	return schedule_list(request)
